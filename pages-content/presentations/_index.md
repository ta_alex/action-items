+++
title = "Presentations and Workshops"
date = 2022-01-04T16:45:32+01:00
weight = 41
toc = true
draft = false
+++






This is a list of relevant presentations and workshops produced by the FST.
Recordings are provided by the Panopto Platform hosted by  TU Darmstadt.

### FAIR Data

#### FAIRe Daten, der Rohstoff des 21ten Jahrhunderts - Enabler für Transparenz, Nachhaltigkeit, Wissenschaft, Wirtschaft

{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=49f4ecf0-158a-484f-a434-ad5d008a8b76&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

#### Forschungsdaten als Ressource
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=ec514276-d8b6-4268-9a4e-ad3600c0413f&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

---
### Datenkompetenz von Anfang an - 4Ing Workshop
This is a series of presentations that were part of two workshops held on 09. October 2023 and 15. April 2024 for the [4Ing Fakultätentage](https://4ing.net/).

#### Teil 1

Digital education and the teaching of data handling skills are becoming increasingly important in the education and training of engineers. In this workshop as part of the 4Ing Faculty Days, Prof. Pelz and his team from TU Darmstadt presented units of the Practical Digitalization module using the example of the Bachelor's degree course in Mechanical Engineering - Sustainable Engineering. It is designed for the digitization focus and, after a test phase, is now going into normal operation with around 300 students.


##### Datenkompetenz von Anfang an
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=04b154f3-bba3-4cf5-a2f9-b13a009b10ed&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

##### Digitaler Baukasten - FAIRe Qualitäts-KPIs für ein LEGO-Fahrzeug
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=73ad21b9-ac76-478b-8b89-b13a009b1108&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

##### Kalorimetrie
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=70c876b8-96ef-4f4c-b2b9-b13a009b113b&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

#### Teil 2

##### Digitale Bildung und Datenkompetenz
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=a4143c01-49ee-4adc-bf3d-b15c00e0e1f5&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

##### Datenkompetenz von Anfang an – Grundlagen der Digitalisierung
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=3f61f7ed-90e9-42c4-afef-b15c00e0e1f5&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

##### FAIRes Prüfstandsdatenmanagement an einem Prüfstand für Fahrwerkskomponenten
{{< rawhtml >}}
<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=2f636e79-9245-44f3-a404-b15300a4f8a4&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

---

### HDF5


#### HDF5-Workshop

Workshop auf Gitlab: [https://git.rwth-aachen.de/nils.preuss/ hdf5-workshop](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop)

{{< rawhtml >}}<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=144884dc-c138-4235-a932-ac9301384c93&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>{{< /rawhtml >}}

---

### FDM in Technical Operations Research (TOR)

#### Vortrag TOR Workshop 08.03.2021: Forschungsdatenmanagement und Technical Operations Research (TOR)

{{< rawhtml >}}<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=2fab9b9b-e29d-41c2-9467-ace700fd1a16&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>{{< /rawhtml >}}



#### Vortrag TOR Workshop 2021: Einblicke in die Lehre zu Technical Operations Research (TOR)

{{< rawhtml >}}<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=079ecff6-f597-4f72-a9ac-ace600e28cbc&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>{{< /rawhtml >}}

---

### Weitere


#### Datenkompetenz von Anfang an (2. NFDI4ING Community Meeting)
30.11-01.12.2020, digitale Konferenz

{{< rawhtml >}}<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=432408c2-30c8-4f17-97a4-acb800ef1b17&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>{{< /rawhtml >}}



#### Userstories zur Interoperabilität und Nachvollziehbarkeit von Daten und Daten-Workflows (2. NFDI4ING Community Meeting)
30.11-01.12.2020, digitale Konferenz

 {{< rawhtml >}}<iframe src="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f34e4237-a70b-43a8-a5aa-acb800eeba69&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>{{< /rawhtml >}}
