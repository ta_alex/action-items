+++
title = "Acknowledgement"
date = 2022-01-04T16:45:32+01:00
weight = 101
toc = false
draft = false
+++





Archetype Alex is organized as one of the task areas of the NFDI4Ing consortium. All action items presented on this website were created as part of the author's work in the consortium.


*The Authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.*

