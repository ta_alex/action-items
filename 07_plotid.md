---
title: 'PlotID - A framework for tracking figures'
status: working
task: [A-2-2, A-2-3, A-2-5, A-3-2] # includes measure
is_published: Yes # Yes, No
internal: No # Yes, No
tags: [Alex, NFDI4ing, Matlab, Python, Data Management, Identifier, Reference]
statusreport_ids: 19 # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Martin Hock
    corresponding_author: true
    orcid: 0000-0001-9917-3152
    affiliation: 1
  - name: Manuela Richter
    orcid: 0000-0003-1060-2622
    affiliation: 1
  - name: Jan Lemmer
    orcid: 0000-0002-0638-1567

affiliations:
 - name: Chair of Fluid Systems, TU Darmstadt
   index: 1
date: 2021-10-14
aliases:
  - /action-items/plotid/
---
# Demo Implementation of the Toolkit PlotID

## Summary
The plotID toolkit supports researchers in tracking and storing relevant data in plots. Plots are labeled with an ID and the corresponding data is stored depending on the researcher's need.

The Software is Open Source and the repositories are available at [GitLab](https://git.rwth-aachen.de/plotid).

**Measures:**
A-2-2, A-2-3, A-2-5, A-3-2
## Description
Figure, plots and diagrams are key for presenting condensed knowledge in the scientific world. However, it is often difficult or impossible to understand which data are shown in an image or and how they were processed. PlotID helps you in two steps:  
1. the plot is labeled with an ID of your choice  
2. the research data, the plotting and further scripts and the plot itself is exported in a folder marked with this ID.   
By referencing the ID along with knowledge of the general storage location (network filesystem, or public repository), it is possible to reproduce the workflow in the future. 
 
<!-- ![Schematic showing the three steps of creating a plot with data and code, tagging the plot with an ID collecting all components for archiving or sharing.](methodology.png)  -->

{{% figure src="methodology.png" title="Methodology of plotID" alt="Schematic showing the three steps of creating a plot with data and code, tagging the plot with an ID collecting all components for archiving or sharing."%}}  

The first implementation was done in Matlab, based on the author's expertise and the prevailing programming languages used at the chair of Fluid Systems.  
Following the most used environments while also moving to an open, non-proprietary platform we are currently working on an implementation in python, which could possibly lead to an executable package that can be accessed by other coding languages.  




### Status
#### Planned Activities

- investigation about tools plotID could be integrated with (software workflows, JupyterLab Notebooks, data validation pipelines...)
- investigation into integrating or accessing existing platforms to use persistent identifiers ([PID](https://support.orcid.org/hc/en-us/articles/360006971013-What-are-persistent-identifiers-PIDs-))
- increased usage of plotID in all relevant student projects that work with data / code and produce figures.
 
- investigation of platform independent adaption of plotID (2023)
- adapting the Matlab version to the open source pendant Octave

#### In-progress Activities

- in-depth testing and bug fixing for the Matlab version
- development of a python version (Q2/2022-2023)
- starting October 2021, the Matlab version has been used in pilot projects by scientists and students
- expanding the automated documentation with examples, guides, instructions for contribution, ...  (Q4/2022-2023)
- investigation and implementation of further applications for the CI-CD technologies (Q3/2022-2023)


#### Completed Activities
- initial implementation in Matlab, covering basic user needs (Q4/2021)
- a stable release was published on [Zenodo](https://doi.org/10.5281/zenodo.5714480) (Q1/2022)
- creation of an automatically generated code documentation for python (Q3/2022) 

## Results

[Matlab version V1](https://git.rwth-aachen.de/plotid/plot_identifier):
- a simple CI/CD test case is implemented in CI/CD using
- a custom Matlab testing environment
- Matlab package, which helps with referencing and exporting plots, data and code

[Python Version (alpha) V0.2.1](https://git.rwth-aachen.de/plotid/plotid_python) in alpha state:
- an object oriented python package that allows tagging and simple copying 
- a PIP package for installation  [PyPI/PlotID](https://pypi.org/project/plotID/)
- a [Readme](https://git.rwth-aachen.de/plotid/plotid_python/-/blob/main/README.md) documenting installation and simple use cases
- GitLab pipeline with multiple CI/CD jobs including unittests and test coverage reporting, linting, API documentation, and security tests 


### Lessons Learned/ Recommendations

- usability is crucial for user acceptance
- comprehensive, easy-to-understand, and reviewed documentation is key for a good user experience
- ease of use requires a lot of user feedback
- object oriented programming should be used if a lot of communication between functions is neccesary
- modularity (as in supporting multiple plot engines) is a key role of OOP

### Publication(s)
- In September 2022 the plotID workflow and packages were presented at the NFDI4Ing community meetings of cluster 41 (mechanical engineering and production technology), 42 (thermal und process engineering) and 45 (architecture and civil engineering). A recording of this presentation can be found under: [DOI: 10.5446/59362](https://doi.org/10.5446/59362)

- In September 2021 the plotID concept was introduced during the NFDI4ING conference. The slides are available (in German) via Zenodo. [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5714480.svg)](https://doi.org/10.5281/zenodo.5714480)

For a detailed documentation, see the [GitLab group](https://git.rwth-aachen.de/plotid) of PlotID.


## Acknowledgements
We acknowledge contributions from the NFDI4ing Team @TU Darmstadt and the pilot users.
