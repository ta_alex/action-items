# action-items

This project is used for the publications of the documentation for action items, that the TA Alex is working on.
Alex is split amongst many participants and has thus further split up its Measures and Tasks into Action Items, on which progress is being documented. 


The action items are presented under https://ta_alex.pages.rwth-aachen.de/action-items with a shorter URL coming soon.
<!-- and you can read up on the progress of this action item under.-->
