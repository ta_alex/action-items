---
title: 'Introduction to Poetry' # Title for the menu / overview page
# menutitle: 'ML-Checklist' # Title to be shown in the menu
status: 'final'
# task: [A-X-X, A-Y-Y] # includes measure
is_published: Yes
internal: No
tags: [Alex, NFDI4ing, Software, Packaging, Python]
statusreport_ids: '-' # ID numbers used in NFDI4ing Status Report (Statusbericht)
# that this article relates to. Use '-' instead of no entry/null/0
authors:
  - name: Daniele Inturri
    affiliation: 1 # (Multiple affiliations must be quoted)
affiliations:
 - name: Chair of Fluid Systems, TU Darmstadt
   index: 1
   # ROR:
   website: https://www.fst.tu-darmstadt.de/fachgebiet/index.de.jsp
date: 2023-12-12
# bibliography: paper.bib #if available
---

# Introduction to Poetry

## Motivation

- efficient Python project management encompasses multiple crucial steps:
  - managing Python versions
  - managing virtual environments
  - managing dependencies
  - package building
  - package publishing
-  [poetry](https://python-poetry.org/) covers almost all these steps

![Tools Diagram](introduction_to_poetry/tools_diagram.png)

[Source](https://www.inovex.de/de/blog/an-unbiased-evaluation-of-environment-management-and-packaging-tools-in-python/)


## Python Version Management

- not supported with [poetry](https://python-poetry.org/)
- instead use [pyenv](https://github.com/pyenv-win/pyenv-win)
- in poetry config set `virtualenvs.prefer-active-python` to `true` to make [pyenv](https://github.com/pyenv-win/pyenv-win) work with [poetry](https://python-poetry.org/)

## Environment Management

- virtual environments crucial for dependency management
- [poetry](https://python-poetry.org/) creates and activates virtual environments for you


## Dependency Management

### Why?

- consistency, stability, collaboration, ...

![works_on_my_machine](introduction_to_poetry/works_on_my_machine.jpg)

### Library vs. Application

- Application: versions of dependencies pinned
- Library: more flexibility needed


## Dependency Management in poetry

- dependencies are specified in pyproject.toml file
- dependencies can be added by running

 ```shell
 poetry add <package_name>
 ```
- [poetry](https://python-poetry.org/) creates a lock file where **all** versions are pinned
- packages can be installed using the lock file
 ```shell
 poetry install
 ```

## Package Building

- before publishing, a package needs to be build
- building will create a source distribution and a built distribution

 ```shell
 poetry build
 ```

## Package Publishing
- last step is publishing the package
- [poetry](https://python-poetry.org/) will publish to PyPI by default
- credentials need to be configured properly
 ```shell
 poetry publish
 ```
