---
#This header contains the meta-data (machine readable)
title: 'Unit Test Workshop at the Chair of Fluid Systems, TU Darmstadt'
status: in progress #planned, paused , in progress, final
task: [A-2-2, A-2-3] # includes measure
is_published: Yes # Yes, No (Was this project published somwhere else?)
internal: No # Yes, No (internal = no publication on website)
tags: [Alex, NFDI4ing, A-2, Lessons Learned, Unit Test, Code Quality]
  #better more than less. (used to connect contents)
statusreport_ids: 54 # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Kevin Logan
    orcid: 0000-0001-5512-2679 # please use ORCID
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Michaela Leštáková
    corresponding_author: true
    orcid: 0000-0002-5998-6754 # please use ORCID
    affiliation: 1
  - name: Daniele Inturri
    affiliation: 1
affiliations:
 - name: Chair of Fluid Systems, TU Darmstadt
   index: 1
date: 2022-10-12 # start date
bibliography: #paper.bib #if needed

---

# Unit Test Workshop at the Chair of Fluid Systems, TU Darmstadt

Unit testing is an important way of achieving higher code quality, especially in collaborative projects.
In order to introduce the topic to our colleagues at the Chair of Fluid Systems, we organized a workshop that was held by Daniele Inturri.
The workshop explained the idea behind unit testing and provided practical information on implementing tests in typical engineering applications.

The slides from the workshop are available [here](presentation_221012_Testen_in_Python_Inturri.pdf) in German.
An English version is available [here](presentation_221027_No-Nonsense_Guide_to_Higher_Code_Quality_Lestakova_Logan_Inturri.pdf). You can find a GitLab repository with the presented tests [here](https://git.rwth-aachen.de/ta_alex/unit-testing-workshop).

Another opportunity to hear the talk on this topic was  at the [NFDI4ing Conference](https://nfdi4ing.de/conference/) which took place on October 26 and 27, 2022.
Michaela Leštáková, Kevin Logan and Daniele Inturri held a workshop titled [A No-Nonsense Guide to Higher Code Quality for Researchers - feat. Unit Testing](https://nfdi4ing.de/conference_abstracts/#857).
