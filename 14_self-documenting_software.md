---
#This header contains the meta-data (machine readable)
title: 'Redesign and development of a test bench control system of a generic exhaust gas test
bench for the investigation of SCR-relevant processes'
title_german: 'Neudesign und Entwicklung einer Prüfstandsteuerung eines generischen Heißgasprüfstandes zur Untersuchung von SCR-relevanten Prozessen'
status: in progress #planned, paused , in progress, final
task: [A-4-2] # A-3-2, A-3-3 ? includes measure
is_published: No # Yes, No (Was this project published somwhere else?)
internal: Yes # Yes, No (internal = no publication on website)
tags: [Alex, NFDI4ing, A-4-2, self-documenting software]
  #better more than less. (used to connect contents)
statusreport_ids: 14 # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Philip Linke
    corresponding_author: false
    orcid:  # please use ORCID
    affiliation: 1 # (Multiple affiliations must be quoted)
  
affiliations:
 - name:  Institute of Reactive Flows and Diagnostics, TU Darmstadt
   index: 1
date: 2023-08-04 # start date
bibliography: #paper.bib #if needed

---
# Redesign and development of a test bench control system of a generic exhaust gas test bench for the investigation of SCR-relevant processes

## Summary

Within the framework of this work, a test bench control system for the existing SCR hot gas test bench of the High Temperature Process Diagnostics (HTPD). For this purpose the actuators and sensors are controlled and read out with the help of the software LabVIEW of the company National Instruments. Control loops are used to automate the operation of the test bench as far as possible by responding by controlling the temperature and flow velocity to a predefined value. The user-friendly design and partially automated control of the test stand will support future tests and simplify operation. Hardware extensions within the scope of this work are intended to increase the degree of automation and reduce the user's workload when operating the test rig.

**Measures:**
A-4-2

## Description

The documentation and storage of measurement data is carried out in conformity with Research Data Management standards in both Technical Data Management Streaming (TDMS) and hierarchical data format 5 (HDF5). A software-based fail-safe control (error handling) reliably intercepts errors. These are classified in terms of their significance for the test bench hardware and appropriate measures are initiated or instructions for action communicated to the user. Operating errors are avoided as far as possible with the aid of a user-oriented graphical interface through bidirectional communication between the user and the software. All data relevant to the experiments on the test bench are displayed in real time via the front panel of the control software, thus enabling easy operation and checking of the system states on the test bench. A manual is also provided so that the user can also check the system states on the test bench. In addition to explaining the basic functions, the structure of software and hardware is described in more detail. In a further step, the procedure for extending and adapting hardware components will be explained in detail. This should enable the developed test bench control to be updated and adapted to new requirements in the future.

### Status

#### planned activities

- Further improvement of the self-documenting software and testing on measurement campaigns.

#### in progress activities

- Testing on measurement campaigns (ongoing).

#### completed activities

- All relevant data for the tests on the test bench are displayed to the user in real time, allowing continuous observation and control of the system conditions.
- User errors are intercepted as much as possible by not displaying options to the user that are inadvisable or non-functional in the current operating state.
- The outstanding feature of the developed solution is the user-friendly and RDM-compliant design. The user-friendliness is evident in the consistent design in combination with the informative feedback of the software to the user.
- A manual, which was created as part of this work, provides the user with instructions for operation, test stand expansion to include additional sensors, and a method guide for dealing with malfunctions and errors.


## Results

Report of the **A**dvanced **D**esign **P**roject group: [here](Ausarbeitung-ADP-Heissgaspruefstand.pdf)

### Lessons Learned/ Recommendations

### Publication(s)

Report of the **A**dvanced **D**esign **P**roject group: [here](Ausarbeitung-ADP-Heissgaspruefstand.pdf)

## Acknowledgements

## References

